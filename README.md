# parrotsec-es.org

Sitio web de Parrot OS en español - Plataforma de aprendizaje

Somos el equipo (the real) de Parrot OS en español.
- Únete a nuestra comunidad en Telegram: https://t.me/ParrotSpanishGroup
- Síguenos en Twitter: https://twitter.com/ParrotSec_es
- Síguenos en Facebook: https://www.facebook.com/parrot.es

![Parrot Wallpaper](/images/Parrot_Background_by_Serverket.png)